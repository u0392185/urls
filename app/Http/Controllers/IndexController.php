<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Mockery\Exception;
const MAX_TRIES = 100;

class IndexController extends Controller
{

    public function index()
    {
        return view('index');
    }

    public function convert(Request $request)
    {
        try{

            $user = auth()->user();

            $url = $request->input('url');
            $url = trim($url, '!"#$%\'()*+,-.@;<=>[\\]^_`{|}~');

            if(strlen($url) > 254){
                $kabloom = explode('?', $url);
                $url = $kabloom[0];
            }

            $valid = $this->validateIt($url);

            if(!$valid){
                abort(400, "Invalid URL");
            }

            $code = $this->createShortCode();

            // Make sure the URL has a scheme.
            $url = $this->urlWithScheme($url, $request);

            \App\Url::create([
                'url' => $url,
                'code' => $code,
                'user_id' => $user->id
            ]);

            $codeUrl = $request->getSchemeAndHttpHost();

            return response()->json([
                'shorty' => "$codeUrl/$code"
            ]);
        } catch (Exception $e) {

            return response($e->getMessage(), 500);
        }
    }

    public function getUserUrls(Request $request){

        $user = auth()->user();

        $urls = [];

        $urlSearch = \App\Url::all()
            ->where('user_id', '=', $user->id);

        foreach ($urlSearch as $url){
            $url->shortUrl = $request->getSchemeAndHttpHost() . "/$url->code";
            array_push($urls, $url);
        }

        return response()->json([
            'urls' => $urls
        ]);
    }

    protected function validateIt($url)
    {
        $regex = "/\b(([\w-]+:?\/?\/?|www[.])[^\s()<>]+(?:\([\w\d]+\)|([^[:punct:]\s]|\/)))/";

        $matched = preg_match($regex, $url);
        if($matched === 0){
            return false;
        }
        return true;
    }

    protected function createShortCode() {

        $code = IndexController::generateShortCode();

        $urls = \App\Url::where('code', $code)->get();

        $resultCount = count($urls);

        if($resultCount === 0){
            $uniqueCodeFound = true;
        }

        return $code;
    }

    public static function generateShortCode($length = 6){

        $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $charactersLength = strlen($characters);
        $randomString = '';
        for ($i = 0; $i < $length; $i++) {
            $randomString .= $characters[rand(0, $charactersLength - 1)];
        }

        return $randomString;
    }

    protected function urlWithScheme($url, Request $request){
        // See if the URL has a scheme.
        $schemePattern = "#^(http|https|ftp)://+#";

        $matched = preg_match($schemePattern, $url);

        if($matched === 0){

            $scheme = $request->getScheme();
            return "$scheme://$url";
        }

        return $url;
    }




}
