<?php

namespace App\Http\Middleware;

use Closure;

class CodeRedirect
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $code = $request->route('code');


        $codeResult = \App\Url::where('code', $code)->first();

        if(isset($codeResult->url)){

            return redirect()->away($codeResult->url);
        }

        return $next($request);
    }
}
