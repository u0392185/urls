@extends('layouts.app')

@section('content')
    <span>
        The short code URL you tried to go to does not exist.
    </span>
@endsection
