<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'IndexController@index')
    ->middleware('auth')
    ->name('index');

Route::get('/{code}', 'CodeController@index')
    ->where('code', '[0-9a-zA-Z]{6}')
    ->middleware(['code']);

Route::post('convert', 'IndexController@convert')->name('code');

Route::get('urls', 'IndexController@getUserUrls')->name('urls');

Auth::routes();

